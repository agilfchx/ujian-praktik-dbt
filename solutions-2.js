s = "aba";
n = 10;

function repeatedString(s, n) {
  var strings = s.repeat(n); // 10x aba
  //   Count Char
  var char_start = 0;
  for (var pos = 0; pos < strings.length; pos++) {
    if (strings.charAt(pos) == "a") {
      char_start += 1;
    }
  }
  return char_start;
}

var output = repeatedString(s, n);
console.log(output);

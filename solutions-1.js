const values = {
  name: "John Doe",
  age: 21,
  username: "johndoe",
  password: "abc123",
  location: "Jakarta",
  time: new Date(),
};

const { password, time, ...rest } = values;

console.log(JSON.stringify(rest));
